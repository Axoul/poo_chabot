package model.agents.animaux;

import java.awt.Point;

import model.agents.Sexe;

public class AbeilleSolitaire extends Abeille{

	public AbeilleSolitaire(Sexe s, Point p) {
		super(s,p);
	}

	public AbeilleSolitaire(Sexe s) {
		super(s, new Point(0,0));
	}

	public AbeilleSolitaire() {
		super(Sexe.Femelle, new Point(0,0));
	}

	public Object clone() {
		return new AbeilleSolitaire(getSexe(), new Point(getCoord().getX(),getCoord().getY()));
	}

}
