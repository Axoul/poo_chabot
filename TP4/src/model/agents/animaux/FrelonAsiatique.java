package model.agents.animaux;

import java.awt.Point;

import model.agents.Sexe;

public class FrelonAsiatique extends Frelon{

	public FrelonAsiatique(Sexe s, Point p) {
		super(s,p);
		proies.add(FrelonEuropeen.class);
	}

	@Override
	public Object clone() {
		return new FrelonAsiatique(getSexe(), new Point(getCoord().getX(),getCoord().getY()));
	}

}
