package model.agents.animaux;

import java.awt.Point;
import java.util.ArrayList;

import model.agents.Agent;
import model.agents.Animal;
import model.agents.Etat;
import model.agents.Sexe;

public abstract class Frelon extends Animal {
	/**
	 * list d'objets de type "Class"
	 * Ces types Class sont paramétrés par <? extends Animal>
	 * Cela signifie que la classe représentée par Class doit hériter de la classe Animal
	 */
	protected ArrayList<Class<? extends Animal>> proies;
	
	public Frelon(Sexe s,Point p) {
		super(s,p);
		proies = new ArrayList<Class<? extends Animal>>();
		proies.add(AbeilleDomestique.class);
		proies.add(AbeilleSolitaire.class);
	}
	
	@Override
	public void rencontrer(Agent a) {
		try {
			gestionProie((Animal)a);
		}
		catch (ClassCastException cce) {
			System.err.println(a+" n'est pas un Animal");
		}
		
	}
	
	protected void gestionProie(Animal a) {
		if(faim && proies.contains(a.getClass())) {
			if(aFaim()) {
				faim = false;
				nbJoursSansManger = 0;
				super.setNiveauSante(Etat.Mourant);
			}
			else {
				super.setNiveauSante(Etat.EnDetresse);
			}
		}
	}
	
	@Override
	protected void maj() {
		if(!faim) {
			if(nbJoursSansManger>=3) {
				faim=true;
			}
			nbJoursSansManger++;
		}
		else {
			if(nbJoursSansManger>=10) {
				aggraverEtat();
			}
		}
	}

	@Override
	protected void seNourrir() {
		if(aFaim()) {
			faim = false;
			nbJoursSansManger = 0;
		}
		
	}
	
}
