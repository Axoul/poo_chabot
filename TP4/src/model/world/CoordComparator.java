package model.world;

import model.agents.Agent;

import java.util.Comparator;

public class CoordComparator implements Comparator<Agent> {

    @Override
    public int compare(Agent a1, Agent a2) {
        int ret = 1;
        if(a1.getCoord().getX()<a2.getCoord().getX()) {ret = -1;}
        else if (a1.getCoord().getX()==a2.getCoord().getX()) {
            if(a1.getCoord().getY()<a2.getCoord().getY()) {
                ret = -1;
            }
        }
        return ret;
    }

}