package model.agents;

import java.awt.Point;

public class PointPositif implements Cloneable{
	private Point p;
	
	public PointPositif(Point p) {
		this.p=new Point();
		this.setX(p.x);
		this.setY(p.y);
	}
	
	public int getX() {return (int)(p.getX());}
	public int getY() {return (int)(p.getY());}
	public boolean setX(int x) {
		boolean ret=true;
		if(x>=0)
			this.p.x=x;
		else {
			this.p.x=0;
			return false;
		}
		return ret;
	}
	public boolean setY(int y) {
		boolean ret=true;
		if(y>=0)
			this.p.y=y;
		else {
			this.p.y=0;
			return false;
		}
		return ret;
	}
	
	@Override
	public String toString() {
		//NomDeLaClasse n° id_animal(sexe, (position x; position y))
		return "("+ getX() + ";" + getY() + ")";
	}
	
	public Object clone() {
		return new PointPositif((Point)p.clone());		
	}
}
