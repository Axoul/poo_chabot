package model.agents.animaux;

import java.awt.Point;

import model.decor.Ruche;
import model.agents.Sexe;

public class AbeilleDomestique extends Abeille {

	public AbeilleDomestique(Sexe s, Point p, Ruche r) {
		super(s,p);
		hebergeur = r;
	}

	public AbeilleDomestique(Sexe s, Ruche r) {
		super(s, new Point(0,0));
		hebergeur = r;
	}

	public AbeilleDomestique(Ruche r) {
		super(Sexe.Femelle, new Point(0,0));
		hebergeur = r;
	}
	
	@Override
	public Object clone() {
		return new AbeilleDomestique(getSexe(), new Point(getCoord().getX(),getCoord().getY()), (Ruche)hebergeur);
	}
	

}
