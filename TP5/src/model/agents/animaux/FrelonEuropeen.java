package model.agents.animaux;

import java.awt.Point;

import model.agents.Sexe;

public class FrelonEuropeen extends Frelon {
	
	public FrelonEuropeen(Sexe s, Point p) {
		super(s,p);
	}


	@Override
	public Object clone() {
		return new FrelonEuropeen(getSexe(), new Point(getCoord().getX(),getCoord().getY()));
	}

}
