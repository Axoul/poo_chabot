package model.agents.vegetaux;

import java.awt.Point;
import java.util.HashSet;

import model.agents.Animal;
import model.agents.animaux.AbeilleDomestique;
import model.agents.animaux.AbeilleSolitaire;
import model.agents.animaux.Frelon;
import model.comportements.Hebergeur;
import model.agents.vegetaux.Vegetal;

public class Arbre extends Vegetal implements Hebergeur{

	private HashSet<Animal> populationArbre;

	public Arbre(Point point, double taille) {
		super(point);
		this.taille=taille;
		populationArbre = new HashSet<Animal>();
	}

	private double taille = 1.0;
	
	@Override
	public boolean peutAccueillir(Animal a) {
		return (a instanceof AbeilleSolitaire || a instanceof Frelon)&&populationArbre.size()<getMaxHeberges();
	}

	private int getMaxHeberges() {
		return (int)(Math.pow(taille,2));
	}

	@Override
	public boolean accueillir(Animal a) {
		boolean ret = false;
		if(peutAccueillir(a)) {
			populationArbre.add((Animal)a);
			ret=true;
		}
		return ret;
	}

	@Override
	public void produire() {
		qteNectar += Math.pow(2, taille);		
	}

	@Override
	public String toString() {
		String ret = "Arbre "+getCoord()+" : population "+populationArbre.size()+" animaux\n";
		for(Animal ad:populationArbre) {
			ret +="\t*"+ad+"\n";
		}
		return ret;
	}


	@Override
	public Object clone() {
		return new Arbre(new Point(getCoord().getX(),getCoord().getY()),taille);
	}

}
