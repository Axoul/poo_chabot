package model.decor;

import java.awt.Point;

import model.agents.PointPositif;
import model.comportements.Dessinable;

public abstract class Decor implements Dessinable {
	/**
	 * coordonnées de l'élément de décor
	 */
	private PointPositif coord;

	public Decor(Point p) {
		coord = new PointPositif(p);
	}

	@Override
	public PointPositif getCoord() {
		return (PointPositif)coord.clone();
	}

	@Override
	public String getImage() {
		return "src/images/"+getClass().getSimpleName()+".gif";
	}

}
