package tp.model.comportements;

import tp.model.agents.Agent;

public interface Rencontre {
    public void rencontrer(Agent a);
}
