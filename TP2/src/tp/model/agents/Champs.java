package tp.model.agents;

import tp.model.comportements.Hebergeur;

import java.awt.Point;

public class Champs extends Decor {

    public Champs(Point coord) {
        super(coord);
    }

    public Champs() {
        super();
    }

    @Override
    public boolean peutAccueillir(Animal a) {
        return false;
    }

    @Override
    public boolean accueillir(Animal a) {
        return false;
    }
}
