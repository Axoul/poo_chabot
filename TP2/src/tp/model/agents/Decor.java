package tp.model.agents;

import tp.model.comportements.Hebergeur;

import java.awt.Point;

public abstract class Decor implements Hebergeur {
    private static int currentId = 0;
    private int id;
    //protected PointPositif coord;
    protected Point coord;

    public Decor(Point coord) {
        id = Decor.getUniqueId();
        this.coord = coord;
    }

    public Decor() {
        id = Decor.getUniqueId();
        this.coord = new Point(0,0);
    }

    public int getId() {
        return id;
    }

    public Point getCoord() {
        return (Point) coord.clone();
    }

    private static int getUniqueId() {
        Decor.currentId++;
        return currentId;
    }
}
