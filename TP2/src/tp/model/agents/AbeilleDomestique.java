package tp.model.agents;

import tp.model.comportements.Rencontre;

import java.awt.*;

public class AbeilleDomestique extends Abeille {

    public AbeilleDomestique(Sexe sexe, Point coord) {
        super(sexe, coord);
    }

    public AbeilleDomestique(Sexe sexe) {
        super(sexe);
    }

    public AbeilleDomestique() {
        super();
    }


}
