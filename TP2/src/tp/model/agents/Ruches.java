package tp.model.agents;

import java.awt.Point;

public class Ruches extends Decor {

    private static int nbAbeilleMax = 50;
    protected int nbAbeille = 0;
    protected int miel = 0;

    public Ruches(Point coord) {
        super(coord);
    }

    public Ruches() {
        super();
    }

    @Override
    public boolean peutAccueillir(Animal a) {
        if(a instanceof AbeilleDomestique) {
            if(nbAbeille < nbAbeilleMax)
                return true;
            else {
                System.out.println("Ruche pleine");
                return false;
            }
        }
        else
            return false;
    }

    @Override
    public boolean accueillir(Animal a) {
        if(peutAccueillir(a)) {
            System.out.println("L'abeille est dans la ruche");
            nbAbeille++;
            miel++;
            return true;
        }
        else
            return false;
    }


}
