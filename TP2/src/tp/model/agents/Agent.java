package tp.model.agents;

import tp.model.comportements.Deplacement;
import tp.model.comportements.Rencontre;

import java.awt.Point;

/**
 * Cette classe modélise un Agent, c'est à dire un élément du monde qui est vivant ET 
 * qui peut interagir avec d'autres éléments de manière réciproque
 * Par exemple, une Abeille (qui butine une fleur) est un Agent
 * Une Fleur (qui produit et donne du nectar ou du pollen à une Abeille) est un Agent.
 * Une ruche, en revanche, n'est pas un agent (elle n'est pas vivante, elle ne produit rien).
 * @author bruno
 *
 */
public abstract class Agent implements Deplacement, Rencontre {
	
	private static int currentId = 0;
	private int id;
	protected int age;
	//protected PointPositif coord;
	protected Point coord;
	
	public Agent(Point coord) {
		age = 0;
		id = Agent.getUniqueId();
		this.coord=coord;
		//this.coord=new PointPositif(coord);
	}

	public int getId() {
		return id;
	}


	public Point getCoord() {
		/*
		 * TP1
		
		Point p = new Point(new Point(coord.getX(),coord.getY()));
		return p;
		*/
		// ou TP2
		return (Point) coord.clone();
	}
	
	protected boolean setAge(int a) {
		boolean ret = false;
		if(a>age) {
			age = a;
			ret = true;
		}
		return ret;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + ((coord == null) ? 0 : coord.hashCode());
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agent other = (Agent) obj;
		if (age != other.age)
			return false;
		if (coord == null) {
			if (other.coord != null)
				return false;
		} else if (!coord.equals(other.coord))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	/**
	 * fait vieillir l'agent d'une unité de temps
	 */
	public void vieillir() {
		setAge(age+1);
	}
	
	/**
	 * 
	 * @param a
	 */
	
	@Override
	public String toString() {
		//NomDeLaClasse n° id_agent (position x; position y)
		return getClass().getSimpleName() + " " + id + " (" + getCoord() + ")";
	}
	
	/* comportements de classe */ 
	/**
	 * Renvoie un identifiant unique non encore utilisé
	 * @return un identifiant entier unique d'animal
	 */
	private static int getUniqueId() {
		//TODO 
		Agent.currentId++;
		return currentId;
	}


}
