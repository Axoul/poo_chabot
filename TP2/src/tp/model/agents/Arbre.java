package tp.model.agents;

import tp.model.comportements.Hebergeur;

import java.awt.Point;

public class Arbre extends Vegetal {

    private int nbHeberge = 0;

    public Arbre(Sexe sexe, Point coord) {
        super(sexe, coord);
    }

    public Arbre(Sexe sexe) {
        super(sexe);
    }

    public Arbre() {
        super();
    }



    @Override
    public boolean peutAccueillir(Animal a) {
        if(a instanceof Frelons || a instanceof AbeilleSolitaire)
            return true;
        else
            return false;
    }

    @Override
    public boolean accueillir(Animal a) {
        if(peutAccueillir(a)) {
            System.out.println("L'insecte est bien dans l'arbre");
            nbHeberge++;
            return true;
        }
        else
            return false;
    }

    @Override
    public void rencontrer(Agent a) {

    }
}
