package tp.model.agents;

import tp.model.comportements.Deplacement;
import tp.model.comportements.Hebergeur;
import tp.model.comportements.Rencontre;

import java.awt.Point;
import java.lang.Math;
import java.util.Objects;

/**
 * Cette classe modélise un Animal dans la simulation
 *
 * @author bruno
 */
public abstract class Animal extends Agent {

    protected Etat etat;
    protected Sexe sexe;
    protected Hebergeur heb;

    /**
     * Constructeur avec 2 paramètres
     *
     * @param sexe
     * @param coord
     */
    public Animal(Sexe sexe, Point coord) {
        super(coord);
        this.sexe = sexe;
        this.etat = Etat.Normal;
    }

    /**
     * Constructeur avec 1 paramètre
     *
     * @param sexe
     */
    public Animal(Sexe sexe) {
        super(new Point(0,0));
        this.sexe = sexe;
        this.etat = Etat.Normal;
    }

    /**
     * Constructeur sans paramètres
     */
    public Animal() {
        super(new Point(0,0));
        this.sexe = Sexe.Femelle;
        this.etat = Etat.Normal;
    }

    /*
     *  Accesseurs et mutateurs
     */

    public Etat getEtat() {
        return etat;
    }

    public Sexe getSexe() {
        return sexe;
    }

    /**
     * Formatage de toString() pour la classe Animal
     *
     * @return Renvoi la String
     */
    @Override
    public String toString() {
        return super.toString() + ", " + sexe;
    }

    /**
     * Permet de comparer l'age, le sexe et l'état de deux classes
     *
     * @param o deuxieme classe comparée
     * @return true si les deux classes partagent les mêmes caractéristique
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Animal))
            return false;
        Animal comp = (Animal) o;
        if (this.age == comp.age && this.sexe == comp.sexe && this.etat == comp.etat)
            return true;
        else
            return false;
    }

    /**
     * Permet de déplacer les animaux de façon aléatoire avec un incrément de +/-1
     */

    @Override
    public void seDeplacer() {
        this.coord.x += Math.round(Math.random()*2-1);
        this.coord.y += Math.round(Math.random()*2-1);
    }
}
