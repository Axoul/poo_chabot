package tp.model.agents;

import java.awt.*;

public class Fleur extends Vegetal {

    public Fleur(Sexe sexe, Point coord) {
        super(sexe, coord);
    }

    public Fleur(Sexe sexe) {
        super(sexe);
    }

    public Fleur() {
        super();
    }

    @Override
    public boolean peutAccueillir(Animal a) {
        return false;
    }

    @Override
    public boolean accueillir(Animal a) {
        return false;
    }

    @Override
    public void rencontrer(Agent a) {

    }
}
