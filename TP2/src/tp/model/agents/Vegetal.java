package tp.model.agents;

import tp.model.comportements.Hebergeur;

import java.awt.Point;

public abstract class Vegetal extends Agent implements Hebergeur {
    protected Etat etat;
    protected Sexe sexe;

    public Vegetal(Sexe sexe, Point coord) {
        super(coord);
        this.sexe = sexe;
        this.etat = Etat.Normal;
    }

    public Vegetal(Sexe sexe) {
        super(new Point(0,0));
        this.sexe = sexe;
        this.etat = Etat.Normal;
    }

    public Vegetal() {
        super(new Point(0,0));
        this.sexe = Sexe.Femelle;
        this.etat = Etat.Normal;
    }


    @Override
    public void seDeplacer() {
        System.out.println("Déplacement impossible");
    }
}
