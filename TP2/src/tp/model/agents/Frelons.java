package tp.model.agents;

import java.awt.Point;

public abstract class Frelons extends Animal {

    public Frelons(Sexe sexe, Point coord) {
        super(sexe, coord);
    }

    public Frelons(Sexe sexe) {
        super(sexe);
    }

    public Frelons() {
        super();
    }


    public abstract void rencontrer(Agent a);

}