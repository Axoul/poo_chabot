package tp.model.agents;

import tp.model.comportements.Hebergeur;
import tp.model.comportements.Rencontre;

import java.awt.Point;

public abstract class Abeille extends Animal implements Hebergeur, Rencontre {

    protected int quantiteMiel = 0;

    public Abeille(Sexe sexe, Point coord) {
        super(sexe, coord);
    }

    public Abeille(Sexe sexe) {
        super(sexe);
    }

    public Abeille() {
        super();
    }

    @Override
    public boolean peutAccueillir(Animal a) {
        if(a instanceof Varroa)
            return true;
        else
            return false;
    }

    @Override
    public boolean accueillir(Animal a) {
        if(peutAccueillir(a)) {
            System.out.println("L'abeille accueil le Varroa");
            a.coord = this.coord;
            //a.heb =
            return true;
        }
        else
            return false;
    }

    @Override
    public void rencontrer(Agent a) {

    }
}
